#include <iostream>
using namespace std;

int main() {

    int N = 10000000;
    int* arr = new int[N];

    for (int i = 0; i < N; i++) {
        arr[i] = 0;
    }

    cout << "DONE" << endl;

    delete[] arr;
 
	return 0;
}
