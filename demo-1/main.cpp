#include <iostream>
using namespace std;

int main() {

    int x = 992684867;
    int* pi = &x;
    char* pc = (char*) &x;

    // cout << "pi: " << (long) pi << endl;
    // cout << "pc: " << (long) pc << endl;
    // cout << "pi-> " << *pi << endl;
    cout << "pc-> " << *pc << endl;
    cout << "pc-> " << *pc + 1 << endl;
    cout << "pc-> " << *pc + 2 << endl;
    cout << "pc-> " << *pc + 3 << endl;


	return 0;
}
